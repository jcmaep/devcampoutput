Rails.application.routes.draw do
  get 'users/index'
  resources :posts
  root to: 'welcome#index'
  match '/users',   to: 'users#index',   via: 'get'
  match '/users/:id',     to: 'users#show',       via: 'get'
  devise_for :users, :path_prefix => 'd'
  resources :users do
    member do
      get :follow
      get :following
      get :followers
    end
  end
  resources :relationships, only: [:create, :destroy]
  

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
